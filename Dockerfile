# Так как мы используем Poetry, для управления зависимостями проекта,
# необходимо использовать Docker multi-stage building:

# The first stage which is named requirements-stage.
FROM python:3.11 as requirements-stage

# Set environment variables
ENV PYTHONDONTWRITEBYTECODE 1
ENV PYTHONUNBUFFERED 1

# Set /tmp as the current working directory. Here's where we will generate the file requirements.txt
WORKDIR /tmp

# Install Poetry in this Docker stage.
RUN pip install poetry

# Copy the pyproject.toml and poetry.lock files to the /tmp directory.
COPY ./pyproject.toml ./poetry.lock* /tmp/

# Generate the requirements.txt file.
RUN poetry export -f requirements.txt --output requirements.txt --without-hashes

# The final stage.
FROM python:3.11

# Set the current working directory to /code.
WORKDIR /code

# Copy the requirements.txt file to the /code directory.
COPY --from=requirements-stage /tmp/requirements.txt /code/requirements.txt

# Install the package dependencies in the generated requirements.txt file.
RUN pip install --no-cache-dir --upgrade -r /code/requirements.txt

# Copy the employee directory to the /code directory.
COPY ./employee /code/employee

# Run the uvicorn command, telling it to use the app object imported from app.main.
CMD ["uvicorn", "employee.main:app", "--host", "0.0.0.0", "--port", "80"]


