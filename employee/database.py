from sqlalchemy import create_engine
from sqlalchemy.orm import sessionmaker
from employee.config import DB_NAME, DB_USER, DB_PORT, DB_PASSWORD, DB_HOST


SQLALCHEMY_DATABASE_URL = f"postgresql://{DB_USER}:{DB_PASSWORD}@{DB_HOST}:{DB_PORT}/{DB_NAME}"

engine = create_engine(SQLALCHEMY_DATABASE_URL)

SessionLocal = sessionmaker(autocommit=False, autoflush=False, bind=engine)


# Создаем зависимость для создания сессий для БД:
def get_db():
    db = SessionLocal()
    try:
        yield db
    finally:
        db.close()
