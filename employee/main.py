from fastapi import FastAPI

from .database import engine
from . import models
from .description import description
from .routers import employees, employee_salary, employee_authentication


app = FastAPI(
    title="EmployeeApp",
    description=description,
    version="0.0.1",
)


app.include_router(employee_authentication.router)
app.include_router(employees.router)
app.include_router(employee_salary.router)


# Создание таблиц в БД:
models.Base.metadata.create_all(bind=engine)


