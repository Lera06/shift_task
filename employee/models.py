from sqlalchemy import Column, ForeignKey, Integer, Float, String, UniqueConstraint
from sqlalchemy.orm import relationship
from sqlalchemy.orm import DeclarativeBase


class Base(DeclarativeBase):
    pass


class Employee(Base):
    __tablename__ = "employee_table"

    id = Column(Integer, primary_key=True, index=True)
    username = Column(String, unique=True, index=True)
    full_name = Column(String)
    email = Column(String, unique=True, index=True)
    hashed_password = Column(String)

    salary = relationship("Salary", back_populates="employee", uselist=False)


class Salary(Base):
    __tablename__ = "salary_table"

    id = Column(Integer, primary_key=True, index=True)
    wage = Column(Float)
    date_of_wage_increase = Column(String)

    employee_id = Column(Integer, ForeignKey("employee_table.id"))

    employee = relationship("Employee", back_populates="salary")

    __table_args__ = (UniqueConstraint("employee_id"), )

