from sqlalchemy.orm import Session
from fastapi import APIRouter, Depends
from employee import crud, schemas
from ..database import get_db


router = APIRouter(
    prefix="/salary",
    tags=["Salary"],
)


@router.post("/{emp_id}", response_model=schemas.Salary, status_code=201)
def create_salary_for_employee(emp_id: int, salary: schemas.SalaryCreate, db: Session = Depends(get_db)):
    return crud.create_employee_salary(db, salary=salary, emp_id=emp_id)

