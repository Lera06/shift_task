from datetime import timedelta
from sqlalchemy.orm import Session
from jose import JWTError, jwt
from fastapi import APIRouter, HTTPException, Depends, status
from fastapi.security import OAuth2PasswordRequestForm
from fastapi.security import OAuth2PasswordBearer
from employee import schemas
from employee.config import SECRET_KEY, ALGORITHM
from ..database import get_db
from ..config import ACCESS_TOKEN_EXPIRE_MINUTES
from ..access_token import create_access_token
from ..crud import get_employee_by_username
from ..hashing import Hash


router = APIRouter(
    prefix="/token",
    tags=["Authentication"],
)


oauth2_scheme = OAuth2PasswordBearer(tokenUrl="token")


def authenticate_employee(username: str, password: str, db: Session = Depends(get_db)):
    employee = get_employee_by_username(db, username)
    # Если сотрудника не существует в нашей БД, возвращается False:
    if not employee:
        return False
    # Если такой сотрудник существует, мы проверяем его пароль:
    if not Hash.verify_password(password, employee.hashed_password):
        return False
    # Если сотрудник существует и введенный им пароль совпадает с его хешем, возвращаем сотрудника:
    return employee


@router.post("/", response_model=schemas.Token)
async def login_for_access_token(form_data: OAuth2PasswordRequestForm = Depends(), db: Session = Depends(get_db)):
    employee = authenticate_employee(form_data.username, form_data.password, db)
    if not employee:
        raise HTTPException(status_code=status.HTTP_401_UNAUTHORIZED,
                            detail="Неверные имя пользователя или пароль",
                            headers={"WWW-Authenticate": "Bearer"},
                            )

    # Иначе создаем токен для данного сотрудника:
    access_token_expires = timedelta(minutes=int(ACCESS_TOKEN_EXPIRE_MINUTES))
    access_token = create_access_token(data={"sub": employee.username}, expires_delta=access_token_expires)
    return schemas.Token(access_token=access_token, token_type="bearer")


# Данная функция будет использоваться как зависимость для функции read_employee_me(),
# чтобы идентифицировать текущего пользователя:
async def get_current_employee_from_token(token: str = Depends(oauth2_scheme), db: Session = Depends(get_db)):
    credentials_exception = HTTPException(
        status_code=status.HTTP_401_UNAUTHORIZED,
        detail="Could not validate credentials",
        headers={"WWW-Authenticate": "Bearer"},
    )
    try:
        # Берем токен, декодируем его, извлекаем из него имя пользователя (username) и
        # проверяем, если такой сотрудник существует в БД:
        payload = jwt.decode(token, SECRET_KEY, algorithms=[ALGORITHM])
        username: str = payload.get("sub")
        print(f"username extracted is {username}")
        if username is None:
            raise credentials_exception
        token_data = schemas.TokenData(username=username)
    except JWTError:
        raise credentials_exception

    # Получив username, мы получаем доступ к текущему сотруднику:
    employee = get_employee_by_username(db, username=token_data.username)
    if employee is None:
        raise credentials_exception
    return employee
