from sqlalchemy.orm import Session
from fastapi import APIRouter, HTTPException, Depends
from employee import crud, schemas
from ..database import get_db
from ..routers.employee_authentication import get_current_employee_from_token


router = APIRouter(
    prefix="/employees",
    tags=["Employees"],
)


@router.get("/me/", response_model=schemas.Employee)
async def read_employee_me(current_employee: schemas.Employee = Depends(get_current_employee_from_token)):
    return current_employee


@router.post("/", response_model=schemas.Employee, status_code=201)
def create_employee(employee: schemas.EmployeeCreate, db: Session = Depends(get_db)):   # Dependency injection
    db_employee_by_email = crud.get_employee_by_email(db, email=employee.email)
    db_employee_by_username = crud.get_employee_by_username(db, username=employee.username)
    if db_employee_by_username:
        raise HTTPException(status_code=400, detail="Пользователь с таким именем уже зарегистрирован.")
    elif db_employee_by_email:
        raise HTTPException(status_code=400, detail="Такой электронный адрес уже зарегистрирован.")
    else:
        return crud.create_employee(db=db, employee=employee)


@router.get("/", response_model=list[schemas.EmployeeList])
def read_employees(skip: int = 0, limit: int = 10, db: Session = Depends(get_db)):
    employees = crud.get_employees(db=db, skip=skip, limit=limit)
    return employees
