from sqlalchemy.orm import Session

from . import models, schemas
from .hashing import Hash


def get_employee(db: Session, emp_id: int):
    return db.query(models.Employee).filter(models.Employee.id == emp_id).first()


def get_employee_by_email(db: Session, email: str):
    employee = db.query(models.Employee).filter(models.Employee.email == email).first()
    return employee


def get_employee_by_username(db: Session, username: str):
    employee = db.query(models.Employee).filter(models.Employee.username == username).first()
    return employee


def get_employees(db: Session, skip: int = 0, limit: int = 10):
    return db.query(models.Employee).offset(skip).limit(limit).all()


def create_employee(employee: schemas.EmployeeCreate, db: Session):
    new_employee = models.Employee(username=employee.username,
                                   full_name=employee.full_name,
                                   email=employee.email,
                                   hashed_password=Hash.get_password_hash(employee.password))
    db.add(new_employee)
    db.commit()
    db.refresh(new_employee)
    return new_employee


def create_employee_salary(db: Session, salary: schemas.SalaryCreate, emp_id: int):
    db_salary = models.Salary(**salary.__dict__, employee_id=emp_id)
    db.add(db_salary)
    db.commit()
    db.refresh(db_salary)
    return db_salary

