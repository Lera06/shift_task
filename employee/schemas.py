from pydantic import BaseModel, EmailStr, Field


class SalaryBase(BaseModel):
    wage: float | None = None
    date_of_wage_increase: str | None = None


class SalaryCreate(SalaryBase):
    pass


class Salary(SalaryBase):
    employee_id: int

    class ConfigDict:
        orm_mode = True


class EmployeeBase(BaseModel):
    username: str
    full_name: str
    email: EmailStr


class EmployeeCreate(EmployeeBase):
    password: str = Field(..., min_length=6)


class Employee(EmployeeBase):
    salary: Salary | None = None

    class ConfigDict:
        orm_mode = True


class EmployeeList(BaseModel):
    full_name: str


class Token(BaseModel):
    access_token: str
    token_type: str


class TokenData(BaseModel):
    username: str | None = None


