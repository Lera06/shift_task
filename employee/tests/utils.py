from sqlalchemy.orm import Session
from fastapi.testclient import TestClient

from ..main import app
from ..crud import get_employee_by_username, create_employee, create_employee_salary
from ..schemas import EmployeeCreate, SalaryCreate


# Создадим тестовое имя пользователя:
TEST_EMPLOYEE_USERNAME = "testusername"
TEST_EMPLOYEE_EMAIL = "user@example.com"


client = TestClient(app)


# Создаем тестового сотрудника, на id которого будем ссылаться в тесте test_create_employee_salary:
def create_dummy_employee(db: Session):
    employee = EmployeeCreate(
        username="user",
        full_name="User Random",
        email="userrandom@example.com",
        password="randompassword"
    )
    employee = create_employee(employee=employee, db=db)

    return employee, employee.id


# Создаем два вспомогательных метода, чтобы получить токен (access_token) для сотрудника с указанным username:

def employee_authentication_headers(username: str, password: str):
    data = {"username": username, "password": password}
    r = client.post("/token", data=data)
    response = r.json()
    auth_token = response["access_token"]
    headers = {"Authorization": f"Bearer {auth_token}"}

    return headers


def authentication_token_from_username(username: str, db: Session):
    password = "secret_password"
    employee = get_employee_by_username(username=username, db=db)

    # Если сотрудник не существует, сначала создаем его:
    if not employee:
        employee_in_create = EmployeeCreate(username=username,
                                            full_name=username,
                                            email=TEST_EMPLOYEE_EMAIL,
                                            password=password)
        employee = create_employee(employee=employee_in_create, db=db)

        # Мы создали сотрудника, теперь можно создать для него зарплату:
        salary_in_create = SalaryCreate(wage=45000,
                                        date_of_wage_increase="2024-05-26",
                                        employee_id=employee.id)
        salary = create_employee_salary(salary=salary_in_create, db=db, emp_id=employee.id)

    return employee_authentication_headers(username=username, password=password)



