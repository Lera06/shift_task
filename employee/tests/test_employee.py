from fastapi.testclient import TestClient
from sqlalchemy import create_engine
from sqlalchemy.orm import sessionmaker
from sqlalchemy.pool import StaticPool
from employee.config import DB_TEST_NAME, DB_TEST_USER, DB_TEST_PORT, DB_TEST_PASSWORD, DB_TEST_HOST
from ..database import get_db
from ..main import app
from .import utils


client = TestClient(app)

SQLALCHEMY_DATABASE_URL = f"postgresql://{DB_TEST_USER}:{DB_TEST_PASSWORD}@{DB_TEST_HOST}:{DB_TEST_PORT}/{DB_TEST_NAME}"

engine = create_engine(SQLALCHEMY_DATABASE_URL, poolclass=StaticPool)

TestingSessionLocal = sessionmaker(autocommit=False, autoflush=False, bind=engine)


# Создаем зависимость, чтобы переопределить get_db зависимость в нашем главном приложении,
# чтобы далее создать сессии для тестовой БД:
def override_get_db():
    try:
        database = TestingSessionLocal()
        yield database
    finally:
        database.close()


# Переопределяем оригинальный метод get_db() тестовым методом override_get_db():
app.dependency_overrides[get_db] = override_get_db


def test_create_employee(db_session):
    response = client.post("/employees/", json={"username": "alice",
                                                "full_name": "Alice Wonders",
                                                "email": "alice@example.com",
                                                "password": "password",
                                                },
                           )
    assert response.status_code == 201, response.text
    data = response.json()
    assert data["username"] == "alice"
    assert data["full_name"] == "Alice Wonders"
    assert data["email"] == "alice@example.com"


def test_create_employee_salary(db_session):
    emp_id = utils.create_dummy_employee(db=db_session)[1]
    response = client.post(f"/salary/{emp_id}", json={"wage": 55000,
                                                      "date_of_wage_increase": "2024-10-01",
                                                      "employee_id": emp_id
                                                      },
                           )
    assert response.status_code == 201, response.text
    data = response.json()
    assert data["wage"] == 55000
    assert data["date_of_wage_increase"] == "2024-10-01"
    assert data["employee_id"] == 1


def test_employee_token_access_to_check_his_confidential_data(db_session, employee_token_headers):

    response = client.get("/employees/me", headers=employee_token_headers)

    assert response.status_code == 200, response.text
    data = response.json()

    # Данные о сотруднике:
    assert data["username"] == utils.TEST_EMPLOYEE_USERNAME           # testusername
    assert data["full_name"] == utils.TEST_EMPLOYEE_USERNAME          # testusername
    assert data["email"] == utils.TEST_EMPLOYEE_EMAIL                 # user@example.com

    # Данные о зарплате:
    assert data["salary"]["wage"] == 45000
    assert data["salary"]["date_of_wage_increase"] == "2024-05-26"
    assert data["salary"]["employee_id"] == 1

