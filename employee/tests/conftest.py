import pytest
from sqlalchemy.orm import Session
from .. import models
from .test_employee import engine, TestingSessionLocal
from .utils import TEST_EMPLOYEE_USERNAME, authentication_token_from_username


# Используем pytest fixture, чтобы между тестами мы могли создать данные для тестовой БД и затем удалить их:
@pytest.fixture()
def db_session():
    models.Base.metadata.create_all(bind=engine)
    db = TestingSessionLocal()

    try:
        yield db
    finally:
        db.close()
        models.Base.metadata.drop_all(bind=engine)


# Метод вернет jwt токен для использования его в тесте - test_employee_token_access_to_check_his_confidential_data:
@pytest.fixture()
def employee_token_headers(db_session: Session):
    return authentication_token_from_username(username=TEST_EMPLOYEE_USERNAME, db=db_session)

