# EmployeeApp
***
Приложение просмотра текущей зарплаты сотрудника и даты следующего повышения, 
используя JSON Web Tokens.

## Технологический стек
***
* FastAPI
* Uvicorn (server)
* Sqlalchemy
* Postgres
* Poetry
* Pytest
* Docker

## Установка
***
Установите [Poetry](https://python-poetry.org/docs/#installing-with-the-official-installer).

Создайте и активируйте виртуальное окружение:

```
poetry init
poetry shell
```

## Запуск проекта локально (без Docker)

```
uvicorn employee.main:app --reload
```

### API доступно по ссылке

```
http://127.0.0.1:8000/docs
```

## Запуск проекта в Docker

Установите [Docker](https://docs.docker.com/engine/install/) и запустите команду:

```
docker-compose up --build
```
